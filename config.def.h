/* See LICENSE file for copyright and license details. */

#define SESSION_FILE "/tmp/dwm-session"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 10;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=9:antialias=true:autohint=true", "Noto Color Emoji:size=8:antialias=true:autohint=true", "Noto Sans Mono CJK JP:size=9:antialias=true:autohint=true" };
static const char dmenufont[]       = "monospace:size=10";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            0,           -1 },
	{ "Firefox",  NULL,       NULL,       0,            0,           -1 },
	{ "mpv",      NULL,       NULL,       0,            1,           -1 },
	{ "Yubico",   NULL,       NULL,       0,            1,           -1 },
	{ "YubiKey",  NULL,       NULL,       0,            1,           -1 },
	{ "steam",    NULL,       "Friends List",       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
#define MOUSE_TO_CYCLE_LAYOUT 0

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */

#ifdef PERTAG
#include "pertag.h"
#endif /* PERTAG */

#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "HHH",      grid },
 	/* { "H[]",      deck },        */
 	/* { "TTT",      bstack },      */
 	/* { "===",      bstackhoriz }, */
 	/* { "###",      nrowgrid },    */
 	/* { "---",      horizgrid },   */
 	/* { ":::",      gaplessgrid }, */
 	/* { "><>",      NULL },        */   /* no layout function means floating behavior */
	{ NULL,       NULL },
 };

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signum>"` */
static Signal signals[] = {
	/* signum       function        argument  */
	{ 1,            setlayout,      {.v = 0} },
	{ 2,            xrdb,           {.v = NULL} },
};

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };

#include "movestack.c"

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                         XK_b,          togglebar,      {0} },
	{ MODKEY|ShiftMask,               XK_j,          movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,               XK_k,          movestack,      {.i = -1 } },
	{ MODKEY,                         XK_j,          focusstack,     {.i = +1 } },
	{ MODKEY,                         XK_k,          focusstack,     {.i = -1 } },
	{ MODKEY,                         XK_p,          incnmaster,     {.i = +1 } },
	{ MODKEY,                         XK_i,          incnmaster,     {.i = -1 } },
	{ MODKEY,                         XK_h,          setmfact,       {.f = -0.05} },
	{ MODKEY,                         XK_l,          setmfact,       {.f = +0.05} },
	{ MODKEY,                         XK_F1,         resetconfig,    {.r = 0 } },
	{ Mod4Mask,                       XK_F1,         resetconfig,    {.r = 1 } },
	{ MODKEY|ShiftMask,               XK_F1,         resetconfig,    {.r = 2 } },
	{ Mod4Mask|ShiftMask,             XK_F1,         resetconfig,    {.r = 3 } },
	{ Mod4Mask|ControlMask,           XK_F1,         resetconfig,    {.r = 4 } },
	{ Mod4Mask|ShiftMask|ControlMask, XK_F1,         resetconfig,    {.r = 5 } },
	{ MODKEY|Mod4Mask,                XK_k,          incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask,                XK_j,          incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,                XK_eacute,     togglegaps,     {0} },
	{ MODKEY|Mod4Mask,                XK_r,          defaultgaps,    {0} },
	{ MODKEY|ShiftMask,               XK_Return,     zoom,           {12} },
	{ MODKEY,                         XK_Tab,        view,           {0} },
	{ MODKEY,                         XK_q,          killclient,     {0} },
	{ MODKEY|ShiftMask,               XK_space,      setlayout,      {0} },
	{ MODKEY|ControlMask,             XK_comma,      cyclelayout,    {.i = -1 } },
	{ MODKEY|ControlMask,             XK_period,     cyclelayout,    {.i = +1 } },
	{ MODKEY,                         XK_space,      togglefloating, {0} },
	{ MODKEY,                         XK_f,          togglefullscr,  {0} },
	{ MODKEY,                         XK_eacute,     view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,               XK_eacute,     tag,            {.ui = ~0 } },
	{ MODKEY,                         XK_F5,         xrdb,           {.v = NULL } },
	TAGKEYS(                          XK_plus,                       0)
	TAGKEYS(                          XK_ecaron,                     1)
	TAGKEYS(                          XK_scaron,                     2)
	TAGKEYS(                          XK_ccaron,                     3)
	TAGKEYS(                          XK_rcaron,                     4)
	TAGKEYS(                          XK_zcaron,                     5)
	TAGKEYS(                          XK_yacute,                     6)
	TAGKEYS(                          XK_aacute,                     7)
	TAGKEYS(                          XK_iacute,                     8)
	{ MODKEY|ShiftMask|ControlMask,   XK_q,          quit,           {0} },
	{ MODKEY|ShiftMask|ControlMask,   XK_r,          quit,           {1} },
	{ MODKEY,                         XK_uring,      focusmon,       {.i = -1 } },
	{ MODKEY,                         XK_section,    focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,               XK_uring,      tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,               XK_section,    tagmon,         {.i = +1 } },

};

	/* { MODKEY|Mod4Mask|ShiftMask,    XK_h,      incrogaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_l,      incrogaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_h,      incrigaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_l,      incrigaps,      {.i = -1 } }, */
	/* { MODKEY,                       XK_y,      incrihgaps,     {.i = +1 } }, */
	/* { MODKEY,                       XK_o,      incrihgaps,     {.i = -1 } }, */
	/* { MODKEY|ControlMask,           XK_y,      incrivgaps,     {.i = +1 } }, */
	/* { MODKEY|ControlMask,           XK_o,      incrivgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,              XK_y,      incrohgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask,              XK_o,      incrohgaps,     {.i = -1 } }, */
	/* { MODKEY|ShiftMask,             XK_y,      incrovgaps,     {.i = +1 } }, */
	/* { MODKEY|ShiftMask,             XK_o,      incrovgaps,     {.i = -1 } }, */

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	#if MOUSE_TO_CYCLE_LAYOUT
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	#endif
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

