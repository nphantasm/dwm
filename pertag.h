/* See LICENSE file for copyright and license details. */

struct Pertag {
	unsigned int curtag, prevtag;               /* current and previous tag */
	int nmasters[LENGTH(tags) + 1];             /* number of windows in master area */
	float mfacts[LENGTH(tags) + 1];             /* mfacts per tag */
	unsigned int sellts[LENGTH(tags) + 1];      /* selected layouts */
	const Layout *ltidxs[LENGTH(tags) + 1][2];  /* matrix of tags and layouts indexes  */
	int showbars[LENGTH(tags) + 1];             /* display bar for the current tag */
	int enablegaps[LENGTH(tags) + 1];           /* enabled/disabled gaps per tag */
	unsigned int gappih[LENGTH(tags) + 1];      /* horizontal gap between windows */
	unsigned int gappiv[LENGTH(tags) + 1];      /* vertical gap between windows */
	unsigned int gappoh[LENGTH(tags) + 1];      /* horizontal outer gaps */
	unsigned int gappov[LENGTH(tags) + 1];      /* vertical outer gaps */
};
